<?php
class First
{
    public $id = 23;
    protected $name = 'Łukasz';

    public function saySomething($word)
    {
        echo $word;
    }
}

class Second extends First
{
    public function getName()
    {
        echo $this->name;
    }
}

$second = new Second;
// echo $second->saySomething('Hello world');

echo $second->getName();