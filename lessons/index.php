<?php
class User{

    private $id;
    private $username;
    private $email;
    private $password;

    public function __construct($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
        //echo 'Wywołanie konstruktora';
    }

    public function register()
    {
        echo 'Użytkownik zalogowany';
    }

    public function login()
    {
     
        $this->auth_user();
    }

    public function auth_user(){
        echo $this->username. ' jest zidentyfikowany.';
    }


    public function __destruct()
    {
        //echo 'Wywołanie destruktora';
    }
}

$User = new User('Łukasz', '1234');

// $User->register();

// $User->login('Łukasz','1234');